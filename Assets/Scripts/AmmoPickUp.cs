using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickUp : MonoBehaviour
{
    [SerializeField] private int ammoCount = 10;

    private void OnTriggerEnter(Collider other)
    {
        Ammo ammo = other.GetComponentInChildren<Ammo>();
        if (!ammo) return;
        ammo.AddAmmo(ammoCount);
        Destroy(gameObject);
    }
}