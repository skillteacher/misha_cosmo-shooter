using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private float penetration = 3f;
    [SerializeField] private float delay = 0.5f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private GameObject sparksEffect;
    [SerializeField] private float sparksLifeTime = 0.1f;
    [SerializeField] private Ammo ammo;
    private float totalDamage;
    private float distanceFactor;
    private bool isReadyToShoot = true;

    private void Update()
    {
        if (Input.GetButton("Fire1") && isReadyToShoot)
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        if (ammo.IsNoAmmo()) return;
        ammo.ReduceAmmo();
        PlayMuzzleFlash();
        Raycasting();
        StartCoroutine(DelayCountdown(delay));
    }

    private void PlayMuzzleFlash()
    {
        muzzleFlashEffect.Play();
    }

    private void Raycasting()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        SummonSparks(hit.point);
        Health targetHealth = hit.transform.GetComponent<Health>();
        if (targetHealth == null) return;
        CountDistanceFactor(hit);
        CountTotalDamage();
        CountArmorFactor(targetHealth);
        targetHealth.TakeDamage(totalDamage);
    }

    private void CountDistanceFactor(RaycastHit hit)
    {
        if(hit.distance>= 10)
        {
            distanceFactor = hit.distance / 10;
        }
        else
        {
            distanceFactor = 1;
        }
    }

    private void CountTotalDamage()
    {
        totalDamage = damage / distanceFactor;
    }

    private void CountArmorFactor(Health targetHealth)
    {
        float targetArmor = targetHealth.armor;
        if(targetArmor > penetration)
        {
            totalDamage /= 2;
        }
        else
        {
            totalDamage *= 2;
        }
    }

    private IEnumerator DelayCountdown(float delay)
    {
        isReadyToShoot = false;
        yield return new WaitForSeconds(delay);
        isReadyToShoot = true;
    }

    private void SummonSparks(Vector3 point)
    {
        GameObject sparks = Instantiate(sparksEffect, point, Quaternion.identity);
        Destroy(sparks, sparksLifeTime);
    }
}