using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float healthAmount = 100f;
    [SerializeField] public float armor = 3;

    public void TakeDamage(float damageAmount)
    {
        healthAmount -= damageAmount;

        if(healthAmount <= 0)
        {
            Destroy(gameObject);
        }
    }
}
