using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    public event Message AnimationAttackMoment;

    public void AnimAttack()
    {
        AnimationAttackMoment?.Invoke();
    }
}
public delegate void Message();
