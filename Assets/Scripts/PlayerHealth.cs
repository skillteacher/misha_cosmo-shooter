using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public event Message PlayerDeath;
    [SerializeField] private float healthAmount = 100f;

    public void TakeDamage(float damageAmount)
    {
        healthAmount -= damageAmount;

        if (healthAmount <= 0)
        {
            PlayerDeath?.Invoke();
        }
    }
}
