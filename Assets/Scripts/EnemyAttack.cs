using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private EnemyAnimation enemyAnimation;
    private Transform target;
    [SerializeField] private float damage = 20f;

    private void Start()
    {
        target = TargetForEnemy.Instance.GetTarget();   
    }

    private void OnEnable()
    {
        enemyAnimation.AnimationAttackMoment += AttakTarget;
    }

    private void OnDisable()
    {
        enemyAnimation.AnimationAttackMoment -= AttakTarget;
    }

    public void AttakTarget()
    {
        target.GetComponent<PlayerHealth>().TakeDamage(damage);
    }
}
