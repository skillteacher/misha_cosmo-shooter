using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TargetForEnemy : MonoBehaviour
{
    [SerializeField] private List<Transform> targets;
    public static TargetForEnemy Instance;

    private void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public void AddTarget(Transform targetTransform)
    {
        targets.Clear();
        targets.Add(targetTransform);
    }

    public Transform GetTarget()
    {
        return targets[0];
    }
}
