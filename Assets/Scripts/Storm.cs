using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storm : MonoBehaviour
{
    [SerializeField] private float damage = 5f;
    
    private void OnTriggerEnter(Collider other)
    {
        CheckingPlayer(other);
    }
    
    private void CheckingPlayer(Collider other)
    {
        if (other.GetComponent<PlayerHealth>())
        {
            other.GetComponent<PlayerHealth>().TakeDamage(damage);
        }
        else return;
    }
}
