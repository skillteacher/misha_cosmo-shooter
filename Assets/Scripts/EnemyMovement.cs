using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    private Transform target;
    [SerializeField] private float visionRange = 10f;
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private float attackDelay = 1f;
    [SerializeField] private Animator animator;
    private bool isRaged;
    private float distanceToTarget;
    private float timer;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        target = TargetForEnemy.Instance.GetTarget();
    }

    private void Update()
    {
        distanceToTarget = Vector3.Distance(target.position, transform.position);

        if (isRaged)
        {
            EngageTarget();
        }
        else if(visionRange >= distanceToTarget)
        {
            isRaged = true;
            animator.SetBool("move", true);
        }
    }


    private void EngageTarget()
    {
        if (navMeshAgent.stoppingDistance >= distanceToTarget)
        {
            AttackTarget();
        }
        else
        {
            ChaseTarget();
        }
    }

    private void ChaseTarget()
    {
        navMeshAgent.SetDestination(target.position);
        animator.SetBool("attack", false);
    }

    private void AttackTarget()
    {
        timer += Time.deltaTime;
        if (timer <= attackDelay) return;
        timer = 0f;
        animator.SetBool("attack", true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, visionRange);
    }
}
