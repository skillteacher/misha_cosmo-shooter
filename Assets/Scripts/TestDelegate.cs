using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDelegate : MonoBehaviour
{
    public delegate void WriteDelegate();
    public event WriteDelegate WriteEvent;
    public bool isEventStart = false;

    private void OnEnable()
    {
        WriteEvent += Write;
        WriteEvent += Write2;
    }

    private void OnDisable()
    {
        WriteEvent -= Write;
        WriteEvent -= Write2;
    }

    private void Update()
    {
        if(isEventStart)
        {
            WriteEvent?.Invoke();
            isEventStart = false;
        }
    }

    private void Write()
    {
        Debug.Log("-=_=-�� ���� �����-=_=-");
    }

    private void Write2()
    {
        Debug.Log("2");
    }
}
//nacxa/lka